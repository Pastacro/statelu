# Contribuer au projet.

Le **projet 404** est né de l'idée qu'il doit toujours être possible de s'impliquer et d'interagir... Merci de vous y interesser.

## Avant de contribuer.

... à rédiger 😉

## Pour contribuer.

... à déterminer...

## Pour communiquer

La chaine [slack](https://join.slack.com/t/polipart/shared_invite/zt-85w3fvc4-XyDUl2ZfcKGPA08P1wAkjA) de Polipart.
