# 404_elus - L'accessibilité des élus en ligne.

<p  align="center"><img  src="images/404_red.png"  width="250px"  /><img  src="images/Data_vis.png"  width="250px"  /></p>

### Statistiques et datavis concernant la présence en ligne des élus (*d'après des données gouvernementales ouvertes & à partager ouvertement*).


# 1. Principe

La motivation vient de l'idée que ceux qui représentent à la fois l'Etat et la population "devraient" être et accessibles en ligne.

Ce n'est pas le cas, une ville sur deux n'a pas de site web (17 000 communes oui).

On créér donc un outil pour avoir un état de l'art du problème, et on le rend accessible à ceux qui ont les moyens de changer les choses (pouvoir publics et dev).

  

## Objectif :

  

1. **Fournir de quoi s'informer** (avec des datavis pertinentes et sexy),

- d'une part sur le nombre d'institutions concernées (par département ou par région),

- et d'autre part sur les personnes impactées (maires, conseillers, citoyens).

2. **Permettre la réutilisation** et l'exploitation des données (institutions, journalistes, écoles de dév, sociologues, chercheurs...).

3. **Créer une courbe de suivi**, comme un indicateur de "santé démocratique".

  

## Diffusion :

Le projet utilise des données publiques pour le grand public, il sera **donc entièrement libre et ouvert.**

On publie le résultat sur [datagouv.fr](https://www.data.gouv.fr/fr/reuses/statistique-sur-la-presence-en-ligne-des-communes-de-france/) indeed, & sur la page des [statistiques](https://www.polipart.fr/statistiques) du projet [polipart.fr](https://www.polipart.fr/), et anywhere else...

On partage le résultat :

* aux écoles de dév et de design (pour que les étudians s'entrainent ou prospectent),

* aux Décodeurs (le monde) + agences régionales FranceTele et RadioFrance,

* aux Ministère des Coll. Territoriales + conseils régionaux et Départementaux,
  

# 2. Réalisation
Il existe une **[première version](https://www.polipart.fr/statistiques)** à remplacer en même temps qu'on ouvre le code et les données. 

1. Consolider les données dispos dans une DB - les sources sont cleans parce que provenant de datagouv.fr

- Identifier les datasets nécéssaires (déjà fait cf liste des chemin d'accès plus-bas).

- Collecter et consolider les datas.

- Automatiser la consolidation dans le temps (ex: mensuelle)

2. Créer l'interface de visualisation.

- Parcours utilisateur.

- Design desktop + mobile.

- Besoin de datavis et choix de l'outil adequat.

3. Permettre la sélection d'un set de données et l'export (ex: toutes les communes sans site du département 41).    
4. Communiquer la réalisation.


## Sources de données.

* **Annuaire mairies** [site web si existant + coord GPS pour vis](https://www.data.gouv.fr/fr/datasets/service-public-fr-annuaire-de-l-administration-base-de-donnees-locales/) (35 OOO fichiers format .xml).

* **Nombre d'élus** par institution [source 1](https://www.vie-publique.fr/questions-reponses/270079-10-questions-sur-les-municipales-2020) /[source 2](https://www.collectivites-locales.gouv.fr/conseil-municipal) (tableur 20 valeurs).

* **Population** [source 1](https://www.data.gouv.fr/fr/datasets/population/)  [source 2](https://www.data.gouv.fr/fr/datasets/base-de-population/) (meilleurs sources à trouver...).

* **Logos départements** [drive](https://drive.google.com/drive/folders/1pPyKBwIJeGhw9Xb-11a-QGiUff3xWMQT?usp=sharing) (img format .jpg).

* **Logos régions** [drive](https://drive.google.com/drive/folders/1MS6rG_4yW501oQ0I9nW1MZX5e2FR6BRP?usp=sharing) (img format .jpg).

## Maquette de réalisation.

... ça arrive *(à faire / à venir)*

## Contraintes.

Ruby et Ruby on rails pour un bonne intégration dans polipart.fr.

Documenter le process.


# 3. Besoins

**People** :

Toutes les bonnes volontées sont bienvenues 😉

Les débutants et experts boivent les mêmes bières 🍺


# 4. Los puntos positivos 👍

**Vous participez à la création d'un service public!** 

**No need office** c'est un projet #covidfriendly #joke - Sinon on pourra travailler à paris au [Liberté Living Lab](https://www.liberte.paris/) anytime. Gros wifi, murs de tableau blancs, et grandes salles de travail... (machines à café et microhum à dispo).

**Contact**: ✉️  [slack](https://join.slack.com/t/polipart/shared_invite/zt-85w3fvc4-XyDUl2ZfcKGPA08P1wAkjA) ou mail Brice Maydieu
